

// Arrays

// Arrays are used top store multple realted data/values in a singlle variable.
// it is created/declare using [] brakcets aloso knoes a "Arrays Literals"

let hobies = ["Play video games ", "Read a book", "listen to music"]

// Arrays make it easy to mange, manipulate a set a of data. arrays have defferent methods/function associated with a arry
//



console.log(typeof hobies);

let grades = [75.4,98.5,90.12,91.50];
const planets = ["Mercury", "Venus", "Mars", "Earth"];

// Arrays as a best practise  contain a values of the same type 

let arraSample =["Sataima", "one punch  Man" , 2500, true];
 console.log(arraSample)



 let dailyRoutines= ["kakain","matulog", "tiktok", "bootcamp", "neflix"];
 let  cities = ["tokyo","manila", "mexeco", "bangkok"];

//  console.log(daily)
 console.log(cities);

 //Each item in a array is called an element 
 //Array as a collection of data,as a convention, iotts name usually plural 

 // we can also add the values of the variable as elements in array

 let unsername1 = "Fighter_smiith";
 let unsername2 = "georgekyle500";
 let unsername3 = "white_night";

 let guilMembers = [unsername1,unsername2,unsername3]

 console.log(guilMembers);



 // .lenght property of an array tells aboutt property, whicch tells us the number of character ia a string 
//  console.log(daily.length);
 console.log(cities.length);


 //whitespaces are counted as character 

 let fullNmame = "Randy Orton";

 console.log(fullNmame.length);

 // we can manipultae the .leght property of an arry. bveing that. length property is a number that tells the total number of an elements in array, we can also delete the last item in an array by maipulating the .length property
// reassign the values of the .leght propert by subtrtacring 1 to its current value 

dailyRoutines.length = dailyRoutines.length-1;
console.log(dailyRoutines.length);



cities.length--;
console.log(cities);


fullNmame = fullNmame.length-1;
console.log(fullNmame);


let thBeatles = ["jonh","paul", "Ringo", "George"];
thBeatles.length++;
console.log(thBeatles);
 

// Accesing the Elements of an array 
// Accesing array elements is one of the more  commob do we with  array



console.log(cities[0]);

let lakerslegend = ["kobe", "shaq", "lebron","Magic", "kaream"];
console.log(lakerslegend[1]);

let currentLaker = lakerslegend[2];
console.log(currentLaker);



//reassign the varible of lakerslegend
lakerslegend [2] = "Pau Gasol";
console.log(lakerslegend);


let favorateFoods = [

    "Tonkatsu",
    "Adobo",
    "Humburger",
    "Sinigang",
    'Pizza'


]
favorateFoods[3] = "letchon";
console.log(favorateFoods);
favorateFoods[4] = "kwek2x";
console.log(favorateFoods);


//  Accesing the last itme/elements in an array 

// we could consisitly acces the last itme in our aray by accesing the index via addig th e.length proprety valuw ,mimus 1 as the index 


console.log(favorateFoods[favorateFoods.length-1]);

let bullsLegends= ["joradan", "pippen", "Rodman","rose","Scalabraine", "kukuc"]

console.log(bullsLegends[bullsLegends.length-1]);

let neWarr = [""];
console.log(neWarr);
console.log(neWarr[0]);

neWarr[0]= "bong";
console.log(neWarr);

console.log(neWarr[1]);
neWarr[1] = "rio";
console.log(neWarr);



//We can add items in our empty array, because we kome the current total number of items in our array.

// so that we do not update the items in the array when we were adding instead ,we can addd itmes at the of the array 


//the current last item of  the array is accesed and updates instead 
// neWarr[2-1]= neWarr[1]
neWarr[neWarr.length-1] = "blair";
 console.log(neWarr)


 //neWarr[2];
 neWarr[neWarr.length] = "jayvee";
 console.log(neWarr);




 // looping over an Arrays

 // we can loop iover anb array anf


 for (let index = 0; index < neWarr.length; index++) {
    
    console.log(neWarr[index]);
    
 }

 let numArr = [5,12,30,46,40];

 // cjeck each item in the array if they are  divisilbyt byn 5 or not


for (let index = 0; index < numArr.length; index++) {


        // loop over ebry


    // to  visulaize firts loop = index = 0 numArr[index] = number[0];
    // to  visulaize firts loop = index = 1 numArr[index] = number[1];
    if (numArr[index] % 5 ===0) {
        console.log(numArr[index] +" is divisiblyt by 5");
    }else{
        console.log(numArr[index] + " is not  divisiblyt by 5");
    }
    
}


// mulitdimersional  Arrays 


// mulitdimersional Aray are arrays that contain othert arrays 
let chessBOard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBOard);

//  Accessinf items in mulitdimersional array is a bit different than 1 dimesional 

// to acces an item in array within as array, first identofy or locate the array where the item is
console.log(chessBOard[1][5]);

// log "a8" in the console form  our chessborad mulitdimersional array 
console.log(chessBOard[7][0]);

// log "h6" in the console form  our chessborad mulitdimersional array 
console.log(chessBOard[5][7]);




